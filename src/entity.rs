use serde::{Serialize, Deserialize};

pub type OcrResult = Vec<Root>;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Root {
    #[serde(rename = "BoxPoints")]
    pub box_points: Vec<BoxPoint>,
    #[serde(rename = "Score")]
    pub score: f64,
    #[serde(rename = "Text")]
    pub text: String,
    #[serde(rename = "cls_label")]
    pub cls_label: i64,
    #[serde(rename = "cls_score")]
    pub cls_score: f64,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct BoxPoint {
    #[serde(rename = "X")]
    pub x: i64,
    #[serde(rename = "Y")]
    pub y: i64,
}