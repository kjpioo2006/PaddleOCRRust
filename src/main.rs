use std::{
    borrow::Borrow,
    env,
    ffi::{CStr, CString, OsString},
    fs::File,
    io::Read,
    path::{Path, PathBuf},
};

use libc::{c_char, c_long, c_uchar, c_void};
use libloading::{Library, Symbol};
use serde::{Deserialize, Serialize};

use crate::entity::OcrResult;
pub mod entity;

// 外部函数签名
type EnableANSIResultFn = unsafe extern "C" fn(bool);
type InitializejsonFn =
    unsafe extern "C" fn(*const c_char, *const c_char, *const c_char, *const c_char, *const c_char);
type DetectFn = unsafe extern "C" fn(*const c_char) -> *const c_char;
type DetectByteFn = unsafe extern "C" fn(*const c_uchar, c_long) -> *const c_char;
type DetectBase64Fn = unsafe extern "C" fn(*const c_char) -> *const c_char;
type FreeEngineFn = unsafe extern "C" fn();

// OCRModelConfig 结构体定义
#[derive(Debug)]
struct OCRModelConfig {
    det_infer: String,
    cls_infer: String,
    rec_infer: String,
    keys: String,
}

// PaddleOCREngine 结构体定义
#[derive(Debug)]
struct PaddleOCREngine {
    paddle_ocr: Library,
    config: OCRModelConfig,
    parameter_json: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[repr(C)]
pub struct OCRParameter {
    pub use_gpu: bool,
    pub gpu_id: i32,
    pub gpu_mem: i32,
    pub cpu_math_library_num_threads: i32,
    pub enable_mkldnn: bool,
    pub det: bool,
    pub rec: bool,
    pub cls: bool,
    pub max_side_len: i32,
    pub det_db_thresh: f32,
    pub det_db_box_thresh: f32,
    pub det_db_unclip_ratio: f32,
    pub use_dilation: bool,
    pub det_db_score_mode: bool,
    pub visualize: bool,
    pub use_angle_cls: bool,
    pub cls_thresh: f32,
    pub cls_batch_num: i32,
    pub rec_batch_num: i32,
    pub rec_img_h: i32,
    pub rec_img_w: i32,
    pub show_img_vis: bool,
    pub use_tensorrt: bool,
}

impl Default for OCRParameter {
    fn default() -> Self {
        OCRParameter {
            use_gpu: false,
            gpu_id: 0,
            gpu_mem: 4000,
            cpu_math_library_num_threads: 10,
            enable_mkldnn: true,
            det: true,
            rec: true,
            cls: false,
            max_side_len: 960,
            det_db_thresh: 0.3,
            det_db_box_thresh: 0.5,
            det_db_unclip_ratio: 1.6,
            use_dilation: false,
            det_db_score_mode: true,
            visualize: false,
            use_angle_cls: true,
            cls_thresh: 0.9,
            cls_batch_num: 1,
            rec_batch_num: 6,
            rec_img_h: 48,
            rec_img_w: 320,
            show_img_vis: false,
            use_tensorrt: false,
        }
    }
}

impl PaddleOCREngine {
    fn new(config: Option<OCRModelConfig>, parameter_json: Option<String>) -> Self {
        let paddle_ocr =
            unsafe { Library::new("PaddleOCR.dll").expect("Failed to load PaddleOCR.dll") };

        // 初始化默认配置
        let config = config.unwrap_or_else(|| {
            let inference_directory = format!("./inference");
            OCRModelConfig {
                det_infer: format!("{}\\ch_PP-OCRv3_det_infer", inference_directory),
                cls_infer: format!("{}\\ch_ppocr_mobile_v2.0_cls_infer", inference_directory),
                rec_infer: format!("{}\\ch_PP-OCRv3_rec_infer", inference_directory),
                keys: format!("{}\\ppocr_keys.txt", inference_directory),
            }
        });

        // 加载参数 JSON 文件
        let parameter_json = parameter_json.unwrap_or_else(|| {
            let root_directory = get_root_directory();
            let parameterjson_path =
                format!("{}\\inference\\PaddleOCR.config.json", root_directory);

            let mut file =
                File::open(&parameterjson_path).expect("Failed to open parameter JSON file");
            let mut contents = String::new();
            file.read_to_string(&mut contents)
                .expect("Failed to read parameter JSON file");

            contents
        });

        // 检查文件和目录是否存在
        if !Path::new(&config.det_infer).exists() {
            panic!("Directory not found: {}", config.det_infer);
        }
        if !Path::new(&config.cls_infer).exists() {
            panic!("Directory not found: {}", config.cls_infer);
        }
        if !Path::new(&config.rec_infer).exists() {
            panic!("Directory not found: {}", config.rec_infer);
        }
        if !Path::new(&config.keys).exists() {
            panic!("File not found: {}", config.keys);
        }

        PaddleOCREngine {
            paddle_ocr,
            config,
            parameter_json,
        }
    }

    fn initializejson(&self) {
        unsafe {
            let enable_ansiresult: Symbol<EnableANSIResultFn> = self
                .paddle_ocr
                .get(b"EnableANSIResult")
                .expect("Failed to find Initializejson");

            let initializejson: Symbol<InitializejsonFn> = self
                .paddle_ocr
                .get(b"Initializejson")
                .expect("Failed to find Initializejson");

            let det_infer = CString::new(self.config.det_infer.clone()).unwrap();
            let cls_infer = CString::new(self.config.cls_infer.clone()).unwrap();
            let rec_infer = CString::new(self.config.rec_infer.clone()).unwrap();
            let keys = CString::new(self.config.keys.clone()).unwrap();
            let parameter_json_cstr = CString::new(self.parameter_json.clone()).unwrap();
            initializejson(
                det_infer.as_ptr(),
                cls_infer.as_ptr(),
                rec_infer.as_ptr(),
                keys.as_ptr(),
                parameter_json_cstr.as_ptr(),
            );

            enable_ansiresult(true);
        }
    }

    fn detect(&self, imagefile: String) {
        unsafe {
            let detect: Symbol<DetectFn> = self
                .paddle_ocr
                .get(b"Detect")
                .expect("Failed to find Detect");

            let c_str = CString::new(imagefile).expect("Failed to create CString");
            let result_ptr = detect(c_str.as_ptr());
            println!("result_ptr>>> {:?}", result_ptr);
            if !result_ptr.is_null() {
                let result_cstr = CStr::from_ptr(result_ptr);
                let result_string = result_cstr.to_string_lossy().into_owned();
                let result = serde_json::from_str::<OcrResult>(&result_string);
                if let Ok(r) = result {
                    r.iter().for_each(|f| {
                        println!("{}", f.text);
                    });
                }
                libc::free(result_ptr as *mut c_void);
            } else {
                // 调用失败处理
                println!("failed");
            }
        }
    }

    fn detect_byte(&self, image_data: Vec<u8>) {
        unsafe {
            let detect_byte: Symbol<DetectByteFn> = self
                .paddle_ocr
                .get(b"DetectByte")
                .expect("Failed to find DetectByte");

            let result_ptr = detect_byte(image_data.as_ptr(), image_data.len() as c_long);
            println!("result_ptr>>> {:?}", result_ptr);
            if !result_ptr.is_null() {
                let result_cstr = CStr::from_ptr(result_ptr);
                let result_string = result_cstr.to_string_lossy().into_owned();
                let result = serde_json::from_str::<OcrResult>(&result_string);
                if let Ok(r) = result {
                    r.iter().for_each(|f| {
                        println!("{}", f.text);
                    });
                }
                libc::free(result_ptr as *mut c_void);
            } else {
                // 调用失败处理
                println!("failed");
            }
        }
    }

    fn detect_base64(&self, image_base64: String) {
        unsafe {
            let detect_base64: Symbol<DetectBase64Fn> = self
                .paddle_ocr
                .get(b"DetectBase64")
                .expect("Failed to find DetectBase64");

            let c_str = CString::new(image_base64).expect("Failed to create CString");
            let result_ptr = detect_base64(c_str.as_ptr());
            if !result_ptr.is_null() {
                let result_cstr = CStr::from_ptr(result_ptr);
                let result_string = result_cstr.to_string_lossy().into_owned();
                let result = serde_json::from_str::<OcrResult>(&result_string);
                if let Ok(r) = result {
                    r.iter().for_each(|f| {
                        println!("{}", f.text);
                    });
                }
                libc::free(result_ptr as *mut c_void);
            } else {
                // 调用失败处理
                println!("failed");
            }
        }
    }

    fn free_engine(&self) {
        unsafe {
            let free: Symbol<FreeEngineFn> = self
                .paddle_ocr
                .get(b"FreeEngine")
                .expect("Failed to find FreeEngine");

            free();
        }
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let parameterjson = serde_json::to_string(&OCRParameter::default()).unwrap();
    let engine = PaddleOCREngine::new(None, Some(parameterjson));
    engine.initializejson();

    let root = get_root_directory();
    let file_path = format!("{}\\imgs\\test.png", root);

    // engine.detect(file_path);

    // let image_data = read_image_file(&file_path).unwrap();
    // engine.detect_byte(image_data);

    let image_base64 = r#"iVBORw0KGgoAAAANSUhEUgAAAlsAAAAyCAYAAABroR7KAAAbAUlEQVR4nO2de3QUVZ7Hf7zzIAnKYxNAQB5BSGZ4hGEwbhCGrJED68S4KCSMzkpyGPKHwytzcnQJI+x4OBOQcc/ZICfoWeQhyAxmXVnEzQ6oKwhDDMwSskBEEwhBAck7gjz2/m5Vdd+6VdVd3VSnu+Pvw2nSXY9bt6ur7v3e3+93f9XtLgMIgiAIgiCIgNA92BUgCIIgCILoypDYIgiCIAiCCCA9a744H+w6EARBEARBdFm6TZw6i2K2CIIgCIIgAgS5EQmCIAiCIAIIiS2CIAiCIIgAQmKLIAiCIAgigPQMdgUIgiDs8HBHK2Q1N0LfO7c79bit3XvA3th+cCSyb6celyCIrgNZtgiCCAuCIbQQPCYemyAIwl863bJ1/+1bkHSjA0bdvAFDbt2E+9jnyDt34C50g47u3eFaj55wsVcvqOkdAaf6RLFRJelBgiAgKEIrFI5NEET402liK5kJrOltzfAj9tecu7xBw9fw72/AI+2tfOnxyGj4KCqGiy+CIAgduW/BlKUA9cnPQoNr4Ssw4dRQ+Ea3zIxFMP7UPPgu+XGgbIMEQQSSgIutod/fhJ+3XOdiyx+mdLTx1zEmuspi7oPrPYIfZrZy2RJ4Yk4Gf//evgOwfuOmINeIIH54jDxYAREVVeonFE6FEBvrXj+87hwMxzfNR6A2+SIMqpsHkSblxLLtBumWNEPzmhQ4vSVAFScI4gdHQJXLo+0tML/pmiNlTWWCC61iO+L6Q0VEtCNl+kPO/Kf4S/zc0PAN7Nj1p6DViSB+eCyCiIGxEPv0w/zT8LokJpDGwGdcIJlbthqGgbTcaNlK2FUBgy6S0CIIwlkCJraebLkOj7U2OVomxnblXr8C98XehvLoWO87BIDBCX9jWBYTEzzxRxA/TEZBLzgCtWsAhhjciB6Ifdht8dIWSZatjnecrSlBEERAos8DIbREnmr+Fma1NQesfE8c//ykYdmZs18Yls189BH4uLwMKo+Ww65tm5kgo2njBOEY6ycDfCAILIzdYqJpGn/Ng0hVVE1TXxPWq9uhS3HYGPiMv9ZBc/MX8I3r8xioPRycdoUgiK6N45YtdB0GUmhp/AMTXDhz8UREVMCPJXLwo09h9dpiHrPV0toK771/gC+TQfeiJrDGJo6CsWNGmQo1giD8YOXjwO+mXPxvEAxaOhDa14zRu//WfwBThu6B4/PfcC8jyxZBEEHA0QdRYzD8S1cvedymZ2wsRI1/iLV503wq+1ZTM1z941641eweeTZ37wFrBw7mSQdDDbRoieQtWUFiiyDugU0NtfoFaM0qepiNGDGg/QD0KjIPgEc63hkDJ1fKS/UxW1q8lnE7hSUJw81XEARBeMFRyxbOOvTE0GUvQPyiX3LB5Q/t1dXQfOSo63PsndvsmI08aN5fUib/GAYnxPPXpYbLUPH5X/lfO/shuL0MliWTOGYU3GX/MJjeU/loDZsxPdVVxpmzNVBR+VdoaWm19X3QijZj+iOuz4c+/tTUzUkQYY2W8mHNER6zdX3Li9Dw0GSYlvI5fDbzRWUbnWXrFZhgazbiOZgAZsKMIAjCfxwTWz+60e4xvcPYLa/DfY+lO3U4F3/b3sLzcF3s1dun/bKfyYJf5T1rK5YK3YTLf7Oav8ft97273bUfiqA5Ty7kf3HZrm2vm4qtguX5rveYKkKevYj7rly6BJ6Ym2HYF8vesWsvbN7ylmUdp0yeAC8XFRiOjd8RLWrrN5aQ6CK6DlvYdY0uQya6hmjLVj4OnzGBNe3gK27B5eJFODlMWYYWrCFQAscvztOLMxVMKTE+N9gzEtPhpU1ZMKJ2L+StK/e+OUEQIY1jAfLT21os16FFKxBCy3Xsdutjm/HyqgIufuwGrWOwuwbGXon74Xtcpm1nJrQ8laeVUVqywVRoaetRNBUsyzddj/uVbtpgeWwUYli+nbp1WXKLYOemX8McJ8rZWgR50uI5hRtslr8QireWQHGuyaqMX0OpSdkhAa9bCexct9CP/cTvhN9/A7xkfqnrt/fnN0PBZRBaCiiypuxapN+2YrKyDC1lB9+C8acqIGKH/0Irbx07R56+nw4P1wKUw+8qLkP0uNnmZfl0nlC4Yb38+P2chN87Sj1KC/X9AT9vdutmcQ92FfBcyOfHheu7K7+p+bVjhLdPwfztfWIF7D1azkNxKo++CWjmWL27HD4umaeuK4PNC8z3xO0qd6+wLHPvqgBW2wuOWLbwETxWVi10GaLrUOT6h+Xw7YH/ghsXL/p0HNGFKPKTjjbYadOViMJDFjUNl7+GS5e+5i64vn2NaRzOnrNnEUKXnx1kNyIKITy2N7LnZ3Ermxj7xS1aTDx6AwUbWr4wdiwcwQZoZoLv+7VVK5aBORcOw8HRWZDDGqrBz62BS6zxyRlnFeGjp/6TfChQO9+80fG8zFLdFukwcThAVdlrsM9bYbkjYQhchoNqedgI5gyvgx1LbOwbNJgoyB4L0e0d0JaQyhr47a7z0WmwTgbWLvK+HSiianiqEqrQ8Y4aHI8WsOQqqE9+AxpyH4UhRYUwra5Q3YO9T1Xiuk6vxDiuczDhIQ+uRBQ6eD4saxAJSdlMPGSbreuAqp0r4HcHbHyRLYehKiULRkxiHe8Bm9Yt7IzTjIMqvIaz8TfLSOeirHOuNcU6l6TNYWo/AzvYvWc89kJITMB71Xv4Bi8zxewe9IDFObEE6xnS9yPCxPiSct4uFufme70f91XWQWZ2KhNxl0PQUjoPNpcvhqkx7iVfvp8Ok9a6P6/W3iyog3ff/xaWL2XiK20zTM/fI5SzAiaMaIVjf9hgPMSqJHiwpRJeXWtc1Vk4IrY8uQ9jH/6pLkYLhdaZ3F85cVgXEXfv8Dqc6uO988xZ8JTuMwqpXCZA0FWHggfTNIigODljW2x9wd2NOBNRi+nS+I99H3KR1dLSxrPOa6Dwk4UWugvRbRjTV7Fo/f2cx3Tbi2ILBZT8fYpfLeHboBDbWLzGJSDxMx4rHN2JpYX57saVd3ZxcIw13PoGFy0FrNf8xNj47MPOCl8o2tgIbwc+V7jhMGQXbndvxBtlgIMW5brcVQlZbHSZxd9yMXc1lXUo7Noz7WD1naterKFIi4T6ilBu2NXvrp0rfo42wEsXbAoGB+kW2xfuNkuxi5o7UaBhfoox55a4nck+bt6A08lvGJbqJuEceA3yAvDd51gNAMa5rzdAoW64PiU8iQV2D3TetaYIAg5eNykWm2X0h37sPvmqUhYBklgT0Z0TM4T7bssaRWiGHVK7IzBzK2vHtA8JrN1JU64Lr4NIi/OmDUqDwx5YnK6JJrRAzbbe9O09sI1tv62mGD5emgObF7B931YsWpkjlE0eZEKscqnyHkVbFhNYq5OHAcQMg+VHy2G5WN5X+2HSMybiLAA4IrZG3fzOcl3U+HG6z1f2BCbTOtbBjthKiNcnJUVLkRZ8jiIExUriGLf4wfQOdoPTtfJQ0MhiC1NEmM1G1B77o4FC6/VSJTYLj1u05vfc7agJJgye18DjyK5BTTgieLzijSU6yxeWFY5iyyk00TancJK0RhkxowXA2JFth4LnVKGR0iR1ZNggxrGGPd8gPjSr1QltOROJUxNYp3JQbdQykmAE60ii00q4yNPQNaRBHWVLQgvBjgvYechmgguMgssfSx23WoIkfE2IeG4uNP3rHujdyQ+FRqG1N7af6TpLgSQjnkPZMsZ/f0UcZK9zpMphxZxJwyC6XbhPXAhijaNcj/2CKgw6E7XdEbEcECpwV7LQZijXZ5NHgY7331RH6+0856+1wixxwdsFMP1t/TYtFaKlS7GWDcS3C4phVv9KePWnBUyoueECLZCVlnBEbA259b3tbcXUDU4y2GYdWlvbPK6X3YhoXQoksihDMbg491ndsuaWFle9xHgxOfYLLWcY+C8iZ7z/YcZtYSM9CRpN3TfS6FESPuKID61S9RVrYDDGVDSq7kkm2oY0VEIBlqtrCBfCDNYJ13/iFh15M7GDdVuB8TOw8rO1jsPSYhcEVNdLm1g/DRRcF7CuJVD8gN6KuG9dJcxg53NGLntvx5rABSi6uTwLLaT3rKnw4R+PhJ4VULaQSvAOT9RqmmVMFV2NkiVW7iCxM0ys8e4qCjkM7rt4yGEDiRx86xIEinW3rbbK6++aty5VccF3VaElni9uPU+Fg5+wdt7EBaobkAnWzn3rVrjPIytPGQhEStu74e1bodkAsxNZ9SZUzh2mXza3nC2TN1wMlUcXG3ZH65V1UiUmuhZNgqsfpeuEVjBwRGzdd/uW3/vanaWIIq36mRxoq6o2XX+/zTqgIEkBt8BBFx267NAahG42WYzYdSH6g1mcllWQvAjWE61Wcl3xM7odPfGDFFsZl+HQJ02Qw8RB6SSzUbFZHI06G8xVBoqCy3AMw3xc1gcUVNgpGjtaLsKwQ9E6SNbw6WLOZCtXyKCJT2zAxUZYctFywaAGXqeJrq3tcLYhFWaOXsjfe0OxapyBMp2QiBc6h3glzo51OhA11t1ZS9SbuI47jYRU3jF6xOxZQg/EcetWtOiW5R0ksOvRLbhLCw8rgfQgf8cQP0+C+45bL+PMrLTx0C/KzIWoBwWocv/EWwoHjaBeC/eCer64pUkdzPF2Q3Qdy5YtdYBmQBBuZudDiYFl7VkoCNe1zwvxWaobUXX/uUBBllxl6fJbvdui7ISpMA7q4Oqjb8JqdpyXpdUt1+rure4+4IjYwmcW+ovdWYoY99XDQ36uCJt1QBed6JZD8YHpGi41fM1FjAjGWfniQvQVf61mmgD0RzjZySHW5eBxKuzFrTE4u6scTug2sA5o1uygXBQ0VCqWBtdaNPNLO7Q3Aab11Y0wUaikoYvyMECa6r7EjpaV19lxTx5RLS1gZs0yRXXzqLPMEtVGvbTmMhuNT2LneTv8zuP+VlYNk7gk7HSYKKtqGgsjGkXBrIhicydf4NH/zr6BllKkvroORmQqswkz0xRXtv66UK4zo+AKl/OkBMBDu8kqnDDSXgeHDljHW7otNN4mF6hluPAQ82WD0BFtRoEpW7Z0uAQZGyxlKBbonaPFmMt4dbJCKMeKmtA/Hn7B/igWKnQTsuHEG5k8ZsuShgKYng/wi5IyWF5eDOfTC4Jm4QrYg6hDFRQbuUuW6wLhtaSmIjzQfGNJgOvytWHZoY8Pe5zVeLzipBCTdULnhkQXqZy/S0QOzg8bLGYTWY5wda5AoYEWApvn6EK2vFm2FFEAUYIFQwuSl11IUcNgYgYG5AtFYQAw2y6PNdzFWr147FOIIZ4fX2Kv5ABkcRZdpYf9cnFiATv3XqwaIt80dkDS8CQmTNRAbx73xkboQZgd6dMMNxVXB46WzbgOJubZdXX1NTjWrwRyskEV5NIEBFfneRgSMcXDFu+/ScicJ4TPwAWXxW2GPLu39jAfDEH1JMhJkb4f/+4YE3kY+mWnepjl6abe9U6O+VJQ3LRgf1Zo0PHBsoWfxfXqPa2kJMG2q8M0vjTUeFB0I2IQe9k1aJnbH0byBersRaiDMmGfmBSjm/FL9e+2/EwYubsclu9eAdu4dWweDMYEBteMx57/dCaMHvWgZd1qvvgSdr1TZrneCkfEVnv37hDtp3Xr8pv/BlHjxxuW94yNMQTXe6uDXcSgdEz7IAbNY0Z4DHL3JFq8YTcAHYUfCiQxTgyFlBYg7w0UTyJYznvvf9j1rFdyZ+7HbEREDMQe3E8MavZm2dI32jozv1i+aqkwTNV3iZiFujKsUlnIIjJ0Rth2KYcTtbOVzt6D2OLni4lQY8MvjOSlCQKXKuugbVwcDFY/ixbHzsfGzEAXessS1htq66Bx3Fj+WZxtmzc6FaYK15B2nkrRLbtELDMczpM68YS1SUPimmBHRRzkuMRkOgyKc7vSeXqCccJgxSUcVvAYyOJsXy1b5ttgLCXGKoW64HDjg2VLGDDp2hjVkoXxpjPVti64MxAFFuDMwkkgZH5wzSJ0swKuLh0DgxesgL1LZytpHCQrlWWAvMrLZZUwi+27d9UGqWw9Rz47Dv3iYmHAAGM6qatXr/H1/uCI2MIHQkffuenXvl/99p8t14347T9B/PO/tFXOtz3sfRV0FWK+Ko1lBUWOz84zcz3KKRs0UNiJqR3wPS4ze7i12b4rl+laX3j19y9DXv6KgLo/wxNs2FkDU+tumNoa8X25MOPHs1gTiXZNob7sCohPTGCdQXWT3qJgQalZUGooBchbMESaQICYiUGl48TO3uKh9A8oMWtVO83iutRzapYu4EAVfJWZBYmYzHGLljrDe2xYYLGeom9uNVVi/Y49dw0Stxr3KD14BqZmp0Ieu4ZKM8L8PKH1ElhHX8NEFtaRDZx2DNgAOTNx4NEfRqB1Qjs3vM5j3YOVAKTY4EH2KDwEkYGWrkzYHxrCwxSbli3J2spzqxVKRQntDk906koD4UP+N6fRzSy0Sv2wAU5+NRsymVjyO10DO05pWhksT8Zck0qs1tWGPYbNausuwv4Df4bZGT/TCS4UWrgc1/uDI2KrvmdvGPa9f2LLE/jwabvYfVzPlBR9XBaKm7599cLm7Lnz9yRWzILqleNEc2G3c/deV/loxRLFFoKCCbfTBBfWGbPUr//DJp5CQgMtWBhXJu6PQff4OCHcF92UOBsR98f3q9cUdw2rF8Y7qbFR9hEDcZUYksYaIQfQ6PNCw4RWCHZTl6mNj0kSS3lUqDXih9ZVAWzKgszCdNgXso23/9i2sgkz7kynlV9g6+V4Nw/MGcA6lKbzirupdjbkpBXBS2xZUtPhkMmhZDw3yixYAxmsAa9GS1U8FJsVxM7dsZlKPjh2R0O0qfXPnNA6T1qsIiYyLXIF7GtxbtjZQ+1+YVCCWfNTmXC0Gqx4Shbrpt5qBZ+kgqJCcsNeZQOCtCw2AILQElyCePJs2VI+Y8xktnpPcVepycDIgJeZtKECxlzxPFpo0XIJLXcwvfVsRD3oTtRivjwhC65vvrkK//lBOVysN5vpYg9HxNa53hHwcIezlhQMiB84z1PSOj01vSJsbSfHSWECUnyZgYIH3YkoaHwBhRTGXok5sRAMzMcXih8tkzuKH3xWomyhQtEkz1bEfFmi2EIwrgy3E3ODYXoIeVajNlMR83aFN2oGaR7n4QM8cWITnD0A7iSKF3AFdghxquVAc/OpjX52ERMMbAQpjbDlvDTKTCl3I74P92WNd3FueZi5/0IACyGNbl/FEomd9X6YiIHP4y6bzgQNFmZWP7QYNMqL2PVUILmVZXAGYuLWVNahWli1wuE8CbGKYHisDFrbmuCYHFO15TzUp1mlDrkXN6Im/IzxSvu2KGIw5O5ZLXwCB3szr0GeJIq8xlX6mpIkFHG5GDEz/H4YuHQ2PLmKiSZ0Ay6IhwFseTWGOCf7WvAwGBjjeQtNcE38cTJUVJ68J6GFOCK2TkVEgpW3QGbo0hegeZr5Y3dEUGj1GTpUt6zdIu1DS/cecKaPPbGFFp+c+Vk6cWIFipg1Rb/hrsfVa5UxKCY5NRzfZBlmccf9zB7/81DiaN1nFHRYhrfH7pg9NgiFHSYy3bJpg9fv1BWSmc4pnA1JUb7n2tESJ2LnxPNdtZ+BEzx1gZIksUBuwDV3x7qFrOOzarC02U5SJ6Dtyzrf0gEhEhcRJvD4JG6ZEZDTZPDAenwTz60/1r9P5+JcbJ3oloyEJHS5HdB/x7A4T57cgLxu8ZBkNcnFZuoQI0zcXpCXCcl5rX4f1z0bnKcjeKYKympS+TM3wyeo/15ohSs1WlZ4FFnp7hmHo5Ogcm4ZbK7JhMWj+0NMyzn4H7ZuZKbnAHkD+PgeFHBenrCHgstft6GMI2ILxc7nEdEw+TvPCUMRfHwPvnwFA+mtEqIejbR+SpkMipM/H/rUltjSQCsRuvBwXxQsaInSEoqieDMTMWixembhYm5NElNNYEC82SxHnmG+4iQXgmj50uqH22Osl6c4LqwXHgvryS1ngshDgYbl4izE8BZbmrDxJSjZDR/xY4oBHCXyJJpsNIhTytG9ohND7sZasQzMVlIYmDRweeuylHgUkynU3FWSG4KjZS/oM6Kb5GuycE1wt2plkuUzA2W3xxATdxCfiWeS4HSwkGFcyw+k5ABTromdW0f6dU04h0mmb46cesD9TEwrtKBmMZmsNpPMLeYUN3j4nSc32mQSU2sVv29G8hQrXusqu/jRlS/dq0oyVLCXD42RpFm0vX+NzkFLXcMthJhmxR2X1VbtIRmsv/nfggBPzZCipkJqqYR3mYDa9na6IS8Wz8nFH9VTzic6t1Ts527B1eAlQN4kcSpu7zFthMN0mzh11l0nCkq8+R0su2aMBxq67AX+uhfaT1fD6adzLMVW0aChcMVmgDxajzQXG85ELHpZ71bj8VGJow0uQHT7mQW4E52B2mk1eYsvsApwV2eCVeTDIRy99quUyhE6xbB4CG0XJYP9fpkj4ewSIRg4BWNw4qCxbD9Apvk1EH5T+TWE6xXcGfvNLaGCFfVUJfQbGWbnyfRRV1Z4euJDoHG3FeEyQDJDcRHK7Zzv2xDO4ZjYQhY1XoEpHXrrVsKif4Thq1/yu0y0aF189V8shVZ5dCz8KfZ+W2XJD5reuWuvqZXJ6oHUJLYIgiAIgvAVR5OalsXcB8nfdUDEXXfOLXzwdNvp036VhzFanp6l+HXPXvyYdpEztuMsvr9UnODB7BootFYuy9dthxYwEloEQRAEQfiDo5Yt5CcdbfB84xUni7RkY/94ONvbXmC8xif//e+mQetWYMwUZpwP73gngiAIgiCChf206zb5S2Q0vOuDtclftvYb4LPQQjCJKQooO2A2eRJaBEEQBEHcC45btjT+rq0JspqvB6JoLrQ+i/TvIc4I5px6Yu5jMGXyRO42FC1dKLDw2YTaDECCIAiCIIh7IWBiC8FUEAuavoW+d247Uh7GaO2M6++XRYsgCIIgCCIYOBogL4O5t2qYMPp5y3VIbb+3DPM46xCD4W936+ZQ7QiCIAiCIAJPQC1bIvjsxLT2Fpja0Qq979o7ZGv3Hjxh6UdRMXClZ68A15AgCIIgCMJ5Ok1saWBEftKNdhh18wYMZgLs/tu3Xaki2rt3h2979ISLPXtDTe8+8H99Ij0XRhAEQRAEEeIE1I1oBsqq/+0TxV8EQRAEQRBdHcdTPxAEQRAEQRBuSGwRBEEQBEEEEBJbBEEQBEEQAYTEFkEQBEEQRAAhsUUQBEEQBBFA/h8+AjbK6AGF2QAAAABJRU5ErkJggg=="#;
    engine.detect_base64(image_base64.to_owned());

    // engine.paddle_ocr.close().unwrap();
    // engine.free_engine();

    Ok(())
}

fn get_root_directory() -> String {
    let current_exe = std::env::current_exe().expect("Failed to get current executable path");
    let root_directory = current_exe
        .parent()
        .expect("Failed to get parent directory")
        .to_path_buf();
    root_directory
        .to_str()
        .expect("Failed to convert root directory path to string")
        .to_string()
}

fn read_image_file(file_path: &str) -> Result<Vec<u8>, std::io::Error> {
    // 打开文件
    let mut file = File::open(file_path)?;

    // 获取文件大小
    let file_size = file.metadata()?.len();

    // 创建一个足够容纳文件数据的 Vec
    let mut buffer = Vec::with_capacity(file_size as usize);

    // 读取文件数据到缓冲区
    file.read_to_end(&mut buffer)?;

    Ok(buffer)
}
